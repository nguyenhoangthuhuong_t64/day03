<?php
$gioiTinh = array(
    0 => 'Nam',
    1 => 'Nữ'
);
$khoa = array(
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'khoa học vật liệu'
);

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="lab3.css">
  </head>
  <body>
  <div id='backDiv'>
    <form  style="position: center !important;
    width:80%;
    margin-left: 10%;
    margin-right: 10%;" >
    
        <div id="nameDiv">
                <label id ="label" class="h-100"
                    for="name">Họ và tên</label>
                <input id="input" class="h-100" type="text" name="name" required>
        </div>
        <div id="infoDiv">
        <label id="label" class="h-100">Giới tính</label>
        <div id="radioBtn">
        <?php
            for ($i = 0; $i <2; $i++) {
            ?>
            <input id="radioBtn"  type="radio" name=$i><?php echo $gioiTinh[$i] ?>
            <?php
            }
            ?> 
        </div>   
        </div>
        <div id="infoDiv">
            <label id="label" class="h-100">Phân khoa</label>
            <select id="input" class="h-100">
                <?php foreach ($khoa  as $i): ?>
                    <option><?php echo $i ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div id="btnDiv">
            <button class="btn btn-success" id="submitId">Đăng ký</button>
        </div>
    
    </form>
    </div>
  </body>
</html>
